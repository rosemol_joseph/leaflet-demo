**LEAFLET**

When you're done, you can delete the content in this README and update the file with details for others getting started with your repository.

*We recommend that you open this README in another tab as you perform the tasks below. You can [watch our video](https://youtu.be/0ocf7u76WSo) for a full demo of all the steps in this tutorial. Open the video in a new tab to avoid leaving Bitbucket.*

---

## LEAFLET

Setting up the project in local

1. Create a folder were we want to create the project. 
2. Download leaflet latest version from https://leafletjs.com/download.html.
3. Copy all the files from leaflet folder to our folder
4. Create index.html file.
	inside that html add 
   ``` <link rel="stylesheet" href="/path/to/leaflet.css" />
       <script src="/path/to/leaflet.js"></script>
   ```
5. reference 
   https://leafletjs.com/reference.html 

   https://leafletjs.com/examples/quick-start/ 

---

**Minimap (interactive)**

   https://github.com/Norkart/Leaflet-MiniMap 

**Discrete zoom levels**

   https://github.com/will4906/leaflet.zoompanel/blob/master/src/L.Control.ZoomPanel.js 
	
**Scalebar**

   https://github.com/daniellsu/leaflet-betterscale 

**Annotations**

   https://github.com/geoman-io/leaflet-geoman
	
**Heatmap**

   https://github.com/Leaflet/Leaflet.heat 
 
demo
	http://leaflet.github.io/Leaflet.heat/demo/draw.html 
	
**Screenshot **
	
https://github.com/pasichnykvasyl/Leaflet.BigImage 

https://github.com/grinat/leaflet-simple-map-screenshoter 
	